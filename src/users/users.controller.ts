import { Body, Controller, Get, NotFoundException, Param, ParseIntPipe, Post, Query } from '@nestjs/common';
import { ApiBadRequestResponse, ApiCreatedResponse, ApiNotFoundResponse, ApiOkResponse, ApiQuery, ApiTags } from '@nestjs/swagger';
import { CreateUserDto } from './dto/create-user.dto';
import { User } from './entities/user.entity';
import { UsersService } from './users.service';

@ApiTags('user')
@Controller('user')
export class UsersController {
    constructor(private usersService: UsersService) { }

    @ApiOkResponse({ type: User, isArray: true, description: 'Buscar todos os usuários' })
    @ApiQuery({ name: 'name', required: false })
    @Get()
    getUsers(@Query('name') name?: string): User[] {
        return this.usersService.findAll(name);
    }

    @ApiOkResponse({ type: User, description: 'Busca usuário por id' })
    @ApiNotFoundResponse()
    @Get(':id')
    getUserById(@Param('id', ParseIntPipe) id: number): User {//ParseIntPipe transfoma o parametro string em int
        const user = this.usersService.findByid(id);

        if (!user) {
            throw new NotFoundException();
        }

        return user;
    }

    @ApiCreatedResponse({ type: User, description: 'Inserir usuário' })
    @ApiBadRequestResponse()
    @Post()
    insertUser(@Body() body: CreateUserDto): User {
        return this.usersService.createUser(body);
    }
}
