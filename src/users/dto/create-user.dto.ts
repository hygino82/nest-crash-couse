import { ApiProperty } from "@nestjs/swagger";
import { IsAlphanumeric, MaxLength } from "class-validator";

export class CreateUserDto {
    @ApiProperty()//adiciona informaçao dos campos requeridos
    @IsAlphanumeric()
    @MaxLength(10)
    name: string;
}