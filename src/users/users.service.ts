import { Injectable } from '@nestjs/common';
import { CreateUserDto } from './dto/create-user.dto';
import { User } from './entities/user.entity';

@Injectable()
export class UsersService {
    users: User[] = [
        {
            id: 1,
            name: 'Juvenal',
        },
        {
            id: 2,
            name: 'Gorete',
        },
        {
            id: 3,
            name: 'Dilma',
        },
        {
            id: 4,
            name: 'Tenório',
        },
        {
            id: 5,
            name: 'Jupira',
        }
    ];

    findByid(userId: number): User {
        return this.users.find(user => user.id === userId);
    }

    findAll(name?: string): User[] {
        if (name) {
            return this.users.filter(user => user.name === name);
        }
        return this.users;
    }

    createUser(createUser: CreateUserDto): User {
        const newUser = {
            id: Date.now(),
            ...createUser
        }

        this.users.push(newUser);
        return newUser;
    }
}
